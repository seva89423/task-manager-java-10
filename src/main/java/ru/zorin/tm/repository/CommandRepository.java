package ru.zorin.tm.repository;

import ru.zorin.tm.api.ICommandRepository;
import ru.zorin.tm.constant.ArgumentConst;
import ru.zorin.tm.constant.TerminalConst;
import ru.zorin.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show developer info"
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show version info"
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show display message"
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show display message"
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.CMD_EXIT, null, "Close application"
    );

    private final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{HELP, ABOUT, VERSION, INFO, EXIT};

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

    public String[] getCommands() {
        return COMMANDS;
    };

    public String[] getArgs() {
        return ARGS;
    };

}