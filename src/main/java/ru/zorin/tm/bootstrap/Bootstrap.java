package ru.zorin.tm.bootstrap;

import ru.zorin.tm.api.ICommandController;
import ru.zorin.tm.api.ICommandRepository;
import ru.zorin.tm.api.ICommandService;
import ru.zorin.tm.constant.ArgumentConst;
import ru.zorin.tm.constant.TerminalConst;
import ru.zorin.tm.controller.CommandController;
import ru.zorin.tm.repository.CommandRepository;
import ru.zorin.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        System.out.println("***WELCOME TO TASK MANAGER***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }
    
    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showInfo();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
        }
    }


    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }


    public static void exit() {
        System.exit(0);
    }

}
