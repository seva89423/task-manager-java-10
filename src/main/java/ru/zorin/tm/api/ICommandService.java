package ru.zorin.tm.api;

import ru.zorin.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
    
}
