package ru.zorin.tm.service;

import ru.zorin.tm.api.ICommandRepository;
import ru.zorin.tm.api.ICommandService;
import ru.zorin.tm.model.TerminalCommand;;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }
    
}
