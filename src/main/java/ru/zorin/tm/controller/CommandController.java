package ru.zorin.tm.controller;

import ru.zorin.tm.api.ICommandController;
import ru.zorin.tm.api.ICommandService;
import ru.zorin.tm.model.TerminalCommand;
import ru.zorin.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.EXIT);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.5");
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

}
